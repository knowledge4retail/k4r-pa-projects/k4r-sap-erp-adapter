<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" indent="yes"/>
	<xsl:param name="hundred" select="100"/>
	<xsl:output method="html" indent="yes"/>
	<xsl:template match="/DELVRY03/IDOC">
		<xsl:variable name="alpha" select="'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
		<xsl:variable name="quote">"</xsl:variable>
		<xsl:text>{</xsl:text>

		<xsl:variable name="PARTNER_ID" >
			<xsl:value-of select="E1EDL20/E1ADRM1/PARTNER_Q['WE']/../PARTNER_ID"/>
		</xsl:variable>
		<xsl:variable name="WADAT" >
			<xsl:value-of select="E1EDL20/Z1EDT01/WADAT"/>
		</xsl:variable>
		<xsl:variable name="WAUHR" >
			<xsl:value-of select="E1EDL20/Z1EDT01/WAUHR"/>
		</xsl:variable>
		<xsl:variable name="CREDAT" >
			<xsl:value-of select="EDI_DC40/CREDAT"/>
		</xsl:variable>
		<xsl:variable name="CRETIM" >
			<xsl:value-of select="EDI_DC40/CRETIM"/>
		</xsl:variable>
		<xsl:variable name="LFDAT" >
			<xsl:value-of select="E1EDL20/Z1EDT01/LFDAT"/>
		</xsl:variable>
		<xsl:variable name="LFUHR" >
			<xsl:value-of select="E1EDL20/Z1EDT01/LFUHR"/>
		</xsl:variable>
		<xsl:variable name="VSTEL_BEZ" >
			<xsl:value-of select="E1EDL20/E1EDL22/VSTEL_BEZ"/>
		</xsl:variable>
		<xsl:variable name="VSTEL" >
			<xsl:value-of select="E1EDL20/VSTEL"/>
		</xsl:variable>
		<xsl:variable name="VBELN" >
			<xsl:value-of select="E1EDL20/VBELN"/>
		</xsl:variable>

		<xsl:text>
	"storeName": "</xsl:text><xsl:value-of select="substring($PARTNER_ID, string-length($PARTNER_ID) - 3)"/>"<xsl:text>,</xsl:text>
		<xsl:choose>
			<xsl:when test="$WADAT != ''">
				"shipTime": "<xsl:value-of select="substring($WADAT, 1, 4)"/>-<xsl:value-of select="substring($WADAT, 5, 2)"/>-<xsl:value-of select="substring($WADAT, 7, 2)"/>T<xsl:value-of select="substring($WAUHR, 1, 2)"/>:<xsl:value-of select="substring($WAUHR, 3, 2)"/>:<xsl:value-of select="substring($WAUHR, 5, 2)"/>+00:00",
			</xsl:when>
			<xsl:otherwise>
				"shipTime": "00000",
			</xsl:otherwise>
		</xsl:choose>
		"creationTime": "<xsl:value-of select="substring($CREDAT, 1, 4)"/>-<xsl:value-of select="substring($CREDAT, 5, 2)"/>-<xsl:value-of select="substring($CREDAT, 7, 2)"/>T<xsl:value-of select="substring($CRETIM, 1, 2)"/>:<xsl:value-of select="substring($CRETIM, 3, 2)"/>:<xsl:value-of select="substring($CRETIM, 5, 2)"/>+00:00",
		"senderQualifier": "<xsl:value-of select="$VSTEL_BEZ"/>",
		"senderId": "<xsl:value-of select="$VSTEL"/>",
		"documentNumber": "<xsl:value-of select="$VBELN"/>",
		"despatchLogisticalUnits": [
		<xsl:for-each select="E1EDL20/E1EDL37">
			{
			<xsl:variable name="VHART" >
				<xsl:value-of select="VHART"/>
			</xsl:variable>
			<xsl:variable name="EXIDV" >
				<xsl:value-of select="EXIDV"/>
			</xsl:variable>
			"parentId": 0,
			"despatchAdviseId": 0,
			"productId": null,
			"packageTypeCode": "<xsl:value-of select="$VHART"/>",
			"logisticUnitId": "<xsl:value-of select="$EXIDV"/>",
			"estimatedDelivery": "<xsl:value-of select="substring($LFDAT, 1, 4)"/>-<xsl:value-of select="substring($LFDAT, 5, 2)"/>-<xsl:value-of select="substring($LFDAT, 7, 2)"/>T<xsl:value-of select="substring($LFUHR, 1, 2)"/>:<xsl:value-of select="substring($LFUHR, 3, 2)"/>:<xsl:value-of select="substring($LFUHR, 5, 2)"/>+00:00",
			"despatchLineItems": [
			<xsl:for-each select="E1EDL44">
				{
				<xsl:variable name="MATNR" >
					<xsl:value-of select="MATNR"/>
				</xsl:variable>
				<xsl:variable name="POSNR" >
					<xsl:value-of select="POSNR"/>
				</xsl:variable>
				<xsl:variable name="VEMNG" >
					<xsl:value-of select="VEMNG"/>
				</xsl:variable>
				"despatchLogisticUnitId": 0,
				"productId": "<xsl:value-of select="$MATNR"/>",
				"requestedProductId": "<xsl:value-of select="$MATNR"/>",
				"lineItemNumber": "<xsl:value-of select="$POSNR"/>",
				"despatchQuantity": "<xsl:value-of select="$VEMNG"/>"

				}<xsl:if test="position() != last()"><xsl:text>,</xsl:text></xsl:if>
			</xsl:for-each>
			]
			}<xsl:if test="position() != last()"><xsl:text>,</xsl:text></xsl:if>
		</xsl:for-each>
		]
		}
	</xsl:template>
</xsl:stylesheet>