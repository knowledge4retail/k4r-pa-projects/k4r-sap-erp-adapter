<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" indent="yes"/>
<xsl:output method="html" indent="yes"/>
<xsl:template match="WPDWGR01/IDOC">

{
"material_groups": [
<xsl:for-each select="E1WPW01/WARENGR[not(.=preceding::WARENGR)]">
{
<xsl:variable name="opCode" select="../AENDKENNZ" />
"material_group" : "<xsl:value-of select="."/>",
"parent_material_group" : "<xsl:value-of select="../E1WPW02/VERKNUEPFG"/>",
"material_group_name" : "<xsl:value-of select="../E1WPW02/BEZEICH"/>",
"operation_code" : 
<xsl:choose>
<xsl:when test="$opCode = 'MODI'">
<xsl:text>"U"</xsl:text>
</xsl:when>
<xsl:when test="$opCode = 'DELE'">
<xsl:text>"D"</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>"I"</xsl:text>
</xsl:otherwise>
</xsl:choose>
}
<xsl:if test="position() != last()">
<xsl:text>,</xsl:text>
</xsl:if>
</xsl:for-each>
]
}
</xsl:template>
</xsl:stylesheet>