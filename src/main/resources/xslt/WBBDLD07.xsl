<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" indent="yes"/>
<xsl:param name="hundred" select="100"/>
<xsl:output method="html" indent="yes"/>
<xsl:template match="/WBBDLD07/IDOC">
<xsl:variable name="alpha" select="'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
<xsl:variable name="quote">"</xsl:variable>
{
	"products":[ 
		<xsl:for-each select="E1WBB01">   
		{
			<xsl:variable name="MATNR_LONG" >
				<xsl:apply-templates select="MATNR_LONG"/>
			</xsl:variable>
			<xsl:variable name="MATNR" >
				<xsl:apply-templates select="MATNR"/>
			</xsl:variable>
			<xsl:variable name="LANG_ISO" >
				<xsl:apply-templates select="LANG_ISO"/>
			</xsl:variable>			
			<xsl:variable name="LOCNR" >
				<xsl:apply-templates select="LOCNR"/>
			</xsl:variable>					
			<xsl:variable name="MTART" >
				<xsl:apply-templates select="E1WBB02/MTART"/>
			</xsl:variable>
			<xsl:variable name="BASME" >
				<xsl:apply-templates select="E1WBB02/BASME"/>
			</xsl:variable>			
			<xsl:variable name="MATKL" >
				<xsl:apply-templates select="E1WBB02/MATKL"/>
			</xsl:variable>	
			<xsl:variable name="MAKTM" >
				<xsl:apply-templates select="E1WBB10/MAKTM"/>
			</xsl:variable>			
			<xsl:variable name="MAABC" >
				<xsl:apply-templates select="E1WBB12/MAABC"/>
			</xsl:variable>			
			<xsl:variable name="ATTYP" >
				<xsl:apply-templates select="E1WBB02/ATTYP"/>
			</xsl:variable>
			"productID"			: "<xsl:value-of    select="$MATNR"/>",
			"productType"		: "<xsl:value-of    select="$MTART"/>",
			"productBaseUnit"	: "<xsl:value-of    select="$BASME"/>",
			"materialGroup"		: "<xsl:value-of    select="$MATKL"/>",			
			"productDescription" : [
				{
					"productID"			: "<xsl:value-of    select="$MATNR"/>",
					"description"	: "<xsl:value-of    select="$MAKTM"/>",
					"isoCode" 		: "<xsl:value-of    select="$LANG_ISO"/>"
				}
			],
			"productUnit": 
			[
				<xsl:for-each select="E1WBB03"> 
				{
					<xsl:variable name="LAENG" >
						<xsl:apply-templates select="LAENG"/>
					</xsl:variable>
					<xsl:variable name="BREIT" >
						<xsl:apply-templates select="BREIT"/>
					</xsl:variable>
					<xsl:variable name="HOEHE" >
						<xsl:apply-templates select="HOEHE"/>
					</xsl:variable>
					<xsl:variable name="MEABM" >
						<xsl:apply-templates select="MEABM"/>
					</xsl:variable>
					<xsl:variable name="BRGEW" >
						<xsl:apply-templates select="BRGEW"/>
					</xsl:variable>
					<xsl:variable name="GEWEI" >
						<xsl:apply-templates select="GEWEI"/>
					</xsl:variable>
					<xsl:variable name="UMREZ" >
						<xsl:apply-templates select="UMREZ"/>
					</xsl:variable>
					<xsl:variable name="UMREN" >
						<xsl:apply-templates select="UMREN"/>
					</xsl:variable>
					<xsl:variable name="MEINH" >
						<xsl:apply-templates select="MEINH"/>
					</xsl:variable>		
					<xsl:variable name="VOLUM" >
						<xsl:apply-templates select="VOLUM"/>
					</xsl:variable>		
					<xsl:variable name="VOLEH" >
						<xsl:apply-templates select="VOLEH"/>
					</xsl:variable>		
					"productID"			: "<xsl:value-of    select="$MATNR"/>",
					"unitCode":"<xsl:value-of   select="MEINH"/>",
					"length":"<xsl:value-of   select="$LAENG"/>",
					"width":"<xsl:value-of    select="$BREIT"/>",
					"height":"<xsl:value-of   select="$HOEHE"/>",
					"dimensionUnit":"<xsl:value-of     select="$MEABM"/>",
					"grossWeight":<xsl:value-of select="$quote"/><xsl:value-of   select="$BRGEW"/>",
					"weightUnit":<xsl:value-of select="$quote"/><xsl:value-of    select="$GEWEI"/>",
					"denominatorBaseUnit":"<xsl:value-of   select="$UMREZ"/>",
					"numeratorBaseUnit":"<xsl:value-of     select="$UMREN"/>",
					"volume":"<xsl:value-of     select="$VOLUM"/>",
					"volumeUnit":"<xsl:value-of     select="$VOLEH"/>",
					"productGtin":[
					<xsl:for-each select="E1WBB04">  
						{
						<xsl:variable name="EAN" >
							<xsl:apply-templates select="EAN11"/>
						</xsl:variable>
						<xsl:variable name="HPEAN" >
							<xsl:apply-templates select="HPEAN"/>
						</xsl:variable>
						<xsl:variable name="EANTP" >
							<xsl:apply-templates select="EANTP"/>
						</xsl:variable>			
						"productID"			: "<xsl:value-of    select="$MATNR"/>",						
						"gtin"				: "<xsl:value-of     select="$EAN"/>",
						"mainGtin"			: "<xsl:value-of     select="$HPEAN"/>",
						"gtinType"			: "<xsl:value-of     select="$EANTP"/>"
						} <xsl:if test="position() != last()"><xsl:text>,</xsl:text></xsl:if> 
					</xsl:for-each>
					]
				}
				<xsl:if test="position() != last()"> 		         		<xsl:text>,</xsl:text>        </xsl:if> 
				</xsl:for-each>	
			],
			"productAttributes": 
			[
				{
				"productID"			: "<xsl:value-of    select="$MATNR"/>",
				"characteristicCode": "MAABC",
				"storeId"			: "<xsl:value-of    select="$LOCNR"/>",
				"propertyLow" 		: "<xsl:value-of    select="$MAABC"/>"
				},
				{
				"productID"			: "<xsl:value-of    select="$MATNR"/>",
				"characteristicCode": "ATTYP",
				"storeId"			: "<xsl:value-of    select="$LOCNR"/>",
				"propertyLow" 		: "<xsl:value-of    select="$ATTYP"/>"
				}
			]
		}
		<xsl:if test="position() != last()"> 		         		<xsl:text>,</xsl:text>        </xsl:if> 
		</xsl:for-each>	
	]
}
</xsl:template>
</xsl:stylesheet>