<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" indent="yes"/>
<xsl:output method="html" indent="yes"/>
<xsl:template match="DEBMAS07/IDOC">

{
"stores": [
<xsl:for-each select="E1KNA1M">
{
  <xsl:variable name="opCode" select="MSGFN" />
  <!--"addressAdditional": "string", -->
  "addressCity": "<xsl:value-of select="ORT01" />",
  "addressCountry": "<xsl:value-of select="LAND1" />",
  "addressPostcode": "<xsl:value-of select="PSTLZ" />",
  <!--"addressState": "string", -->
  "addressStreet": "<xsl:value-of select="STRAS" />",
  <!--"addressStreetNumber": "string", -->
  <!--  "cadPlanId": "string", -->
  <!-- "latitude": 0, -->
  <!--"longitude": 0, -->
  "storeName": "<xsl:value-of select="NAME1" />",
  "storeNumber": "<xsl:value-of select="KUNNR" />",
  "operation_code" : 
  <xsl:choose>
	<xsl:when test="$opCode = '009'">
		<xsl:text>"I"</xsl:text>
	</xsl:when>
	<xsl:when test="$opCode = '003'">
		<xsl:text>"D"</xsl:text>
	</xsl:when>
	<xsl:otherwise>
		<xsl:text>"U"</xsl:text>
	</xsl:otherwise>
  </xsl:choose>
}
	<xsl:if test="position() != last()">
		<xsl:text>,</xsl:text>
	</xsl:if>
</xsl:for-each>
]
}
</xsl:template>
</xsl:stylesheet>