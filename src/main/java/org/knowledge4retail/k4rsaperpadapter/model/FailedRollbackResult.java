package org.knowledge4retail.k4rsaperpadapter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FailedRollbackResult {

    private String id;
    private String type;
    private String entityKey;
    private String reason;

}
