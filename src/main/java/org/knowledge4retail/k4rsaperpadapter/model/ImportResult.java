package org.knowledge4retail.k4rsaperpadapter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImportResult {

    public enum Status {
        SUCCESSFUL, FAILED
    }

    private String entityId;
    private Status status;
    private FailedImportResult failedImportResult;

}
