package org.knowledge4retail.k4rsaperpadapter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FailedImportResult {

    private String id;
    private String message;
    private boolean successfulRollback;
    private List<FailedRollbackResult> failedRollbackResults = new ArrayList<>();

}
