package org.knowledge4retail.k4rsaperpadapter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportDataRequestDto {

    private List<Object> data;

}
