package org.knowledge4retail.k4rsaperpadapter;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.impl.CompositeReadXSLTStore;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.impl.InMemoryXSLTStore;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.ReadWriteXSLTStore;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.impl.ResourcesReadXSLTStore;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@OpenAPIDefinition(
        info = @Info(title = "K4R SAP ERP Adapter API", version = "1", description = "K4R SAP ERP adapter API reference")
)
@ConfigurationPropertiesScan("org.knowledge4retail.k4rsaperpadapter")
@SpringBootApplication
public class K4rSapErpAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(K4rSapErpAdapterApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ReadWriteXSLTStore xsltStore() {
        InMemoryXSLTStore inMemoryStore = new InMemoryXSLTStore();

        return new CompositeReadXSLTStore()
                .addReadStore(new ResourcesReadXSLTStore())
                .addReadStore(inMemoryStore)
                .setWriteStore(inMemoryStore);
    }
}
