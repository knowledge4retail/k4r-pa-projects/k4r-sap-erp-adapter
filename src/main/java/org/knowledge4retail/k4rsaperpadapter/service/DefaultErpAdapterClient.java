package org.knowledge4retail.k4rsaperpadapter.service;

import org.knowledge4retail.k4rsaperpadapter.model.ImportOperationResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DefaultErpAdapterClient implements ErpAdapterClient{

    private final RestTemplate restTemplate;

    @Value("${erpadapter.url}")
    private String baseUrl;

    public DefaultErpAdapterClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public ImportOperationResult sendProductImportRequest(String logicalSystemId, String payload) {
        RequestEntity<String> requestEntity = RequestEntity
                .post(baseUrl + "/import/logicalsystem/" + logicalSystemId + "/product")
                .contentType(MediaType.APPLICATION_JSON)
                .body(payload);

        return restTemplate.exchange(requestEntity, ImportOperationResult.class).getBody();
    }

    @Override
    public ImportOperationResult sendDespatchAdviceImportRequest(String logicalSystemId, String payload) {
        RequestEntity<String> requestEntity = RequestEntity
                .post(baseUrl + "/import/logicalsystem/" + logicalSystemId + "/despatchadvice")
                .contentType(MediaType.APPLICATION_JSON)
                .body(payload);

        return restTemplate.exchange(requestEntity, ImportOperationResult.class).getBody();
    }
}
