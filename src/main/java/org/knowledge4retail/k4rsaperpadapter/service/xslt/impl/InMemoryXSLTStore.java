package org.knowledge4retail.k4rsaperpadapter.service.xslt.impl;

import org.knowledge4retail.k4rsaperpadapter.exception.XSLTWriteException;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.ReadWriteXSLTStore;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Simple implementation of {@link ReadWriteXSLTStore} that uses
 * an {@link ConcurrentMap} for storing XSLT data in memory.
 */
public class InMemoryXSLTStore implements ReadWriteXSLTStore {

    private final ConcurrentMap<String, String> store = new ConcurrentHashMap<>();

    @Override
    public Source readXSLT(String type) {
        String xslt = store.get(type);

        if(xslt == null)  return null;

        return new StreamSource(new ByteArrayInputStream(xslt.getBytes()));
    }

    @Override
    public void writeXSLT(String type, String xsltData) throws XSLTWriteException {
        if(type == null || type.isBlank() || xsltData == null || xsltData.isBlank())
            throw new XSLTWriteException("Type or xslt data is null and cannot be written to the store!");

        store.put(type, xsltData);
    }
}
