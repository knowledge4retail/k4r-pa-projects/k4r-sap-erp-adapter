package org.knowledge4retail.k4rsaperpadapter.service.xslt;

import javax.xml.transform.Source;

/**
 * Interface for accessing XSLT files.
 */
public interface ReadOnlyXSLTStore {

    /**
     * Tries to find the XSLT {@link Source} for a given IDOC type.
     * Returns null, if no XSLT source could be found.
     *
     * @param type type of IDOC
     * @return XSLT {@link Source} for IDOC transformation
     */
    Source readXSLT(String type);

}
