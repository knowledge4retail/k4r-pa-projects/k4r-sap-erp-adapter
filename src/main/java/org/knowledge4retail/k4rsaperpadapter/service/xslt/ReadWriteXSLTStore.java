package org.knowledge4retail.k4rsaperpadapter.service.xslt;

import org.knowledge4retail.k4rsaperpadapter.exception.XSLTWriteException;

/**
 * Interface for reading and writing XSLT files.
 */
public interface ReadWriteXSLTStore extends ReadOnlyXSLTStore {

    /**
     * Write the xslt for a given IDOC type to the store.
     *
     * @param type     type of IDOC
     * @param xsltData xslt data for transformation
     * @throws XSLTWriteException if write operation fails
     */
    void writeXSLT(String type, String xsltData) throws XSLTWriteException;

}
