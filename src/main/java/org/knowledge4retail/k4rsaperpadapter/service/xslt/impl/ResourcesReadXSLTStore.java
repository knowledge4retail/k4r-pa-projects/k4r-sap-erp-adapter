package org.knowledge4retail.k4rsaperpadapter.service.xslt.impl;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.ReadOnlyXSLTStore;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Reads XSLT files from the classpath.
 * SAP standard IDOC transformations are intended to be put in the
 * src/main/resources/xslt folder. These XSLT files are bundled and
 * shipped as default transformations for standard IDOCs.
 *
 */
@Slf4j
public class ResourcesReadXSLTStore implements ReadOnlyXSLTStore {

    @Override
    public Source readXSLT(String type) {
        try {
            Resource resource = new ClassPathResource("xslt/"+type+".xsl");
            Reader reader = new InputStreamReader(resource.getInputStream(), "UTF-8");

            return new StreamSource(reader);
        } catch (IOException e) {
            log.info("{} could not find file {}.xsl", this.getClass().getSimpleName(), type);
            return null;
        }
    }
}
