package org.knowledge4retail.k4rsaperpadapter.service.xslt.impl;

import org.knowledge4retail.k4rsaperpadapter.exception.XSLTWriteException;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.ReadOnlyXSLTStore;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.ReadWriteXSLTStore;

import javax.xml.transform.Source;
import java.util.LinkedList;
import java.util.List;

/**
 * Special implementation of a {@link ReadWriteXSLTStore} that chains
 * together several {@link ReadOnlyXSLTStore} for looking up xslt data
 * in multiple stores.
 * <p/>
 * This implementation uses a delegate {@link ReadWriteXSLTStore} for storing
 * XSLT data.
 */
public class CompositeReadXSLTStore implements ReadWriteXSLTStore {

    private final List<ReadOnlyXSLTStore> readStores = new LinkedList<>();
    private ReadWriteXSLTStore delegate;

    /**
     * Adds another {@link ReadOnlyXSLTStore} to the store chain.
     * Stores will later be searched in order of their
     *
     * @param readOnlyStore {@link ReadOnlyXSLTStore}
     * @param <T>           any store that extends {@link ReadOnlyXSLTStore}
     * @return this for method chaining
     */
    public <T extends ReadOnlyXSLTStore> CompositeReadXSLTStore addReadStore(T readOnlyStore) {
        readStores.add(readOnlyStore);

        return this;
    }

    /**
     * Sets a delegate {@link ReadWriteXSLTStore} for storing the actual XSLT data.
     *
     * @param writeStore delegate which is used for actual storing
     * @return this for method chaining
     */
    public CompositeReadXSLTStore setWriteStore(ReadWriteXSLTStore writeStore) {
        this.delegate = writeStore;

        return this;
    }

    @Override
    public Source readXSLT(String type) {
        for (ReadOnlyXSLTStore readStore : readStores) {
            Source source = readStore.readXSLT(type);

            if (source != null) {
                return source;
            }
        }

        return null;
    }

    @Override
    public void writeXSLT(String type, String xsltData) throws XSLTWriteException {
        if (delegate == null)
            throw new NullPointerException("Write store is null. Call setWriteStore of " +
                    this.getClass().getSimpleName() + " before writing any data to it!");

        delegate.writeXSLT(type, xsltData);
    }
}
