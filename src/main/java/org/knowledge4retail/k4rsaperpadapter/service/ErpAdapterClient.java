package org.knowledge4retail.k4rsaperpadapter.service;

import org.knowledge4retail.k4rsaperpadapter.model.ImportOperationResult;

public interface ErpAdapterClient {

    ImportOperationResult sendProductImportRequest(String logicalSystemId, String payload);

    ImportOperationResult sendDespatchAdviceImportRequest(String logicalSystemId, String payload);

}
