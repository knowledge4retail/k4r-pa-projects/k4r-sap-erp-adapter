package org.knowledge4retail.k4rsaperpadapter.service;

import org.knowledge4retail.k4rsaperpadapter.exception.XMLTransformException;
import org.knowledge4retail.k4rsaperpadapter.model.ImportOperationResult;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public interface ImportService {


    ImportOperationResult importData(String xmlData) throws XMLTransformException;

}
