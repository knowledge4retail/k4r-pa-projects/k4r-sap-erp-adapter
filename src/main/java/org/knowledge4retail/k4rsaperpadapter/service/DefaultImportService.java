package org.knowledge4retail.k4rsaperpadapter.service;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.k4rsaperpadapter.exception.XMLTransformException;
import org.knowledge4retail.k4rsaperpadapter.model.ImportOperationResult;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.ReadWriteXSLTStore;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;

@Slf4j
@Service
public class DefaultImportService implements ImportService {

    private final ReadWriteXSLTStore xsltStore;
    private final ErpAdapterClient adapterClient;

    public DefaultImportService(ReadWriteXSLTStore xsltStore, ErpAdapterClient adapterClient) {
        this.xsltStore = xsltStore;
        this.adapterClient = adapterClient;
    }

    @Override
    public ImportOperationResult importData(String xmlData) throws XMLTransformException {
        try {
            Document xmlDocument = getXmlDocument(xmlData);
            String idocType = getFromXPath(xmlDocument, "//IDOC/EDI_DC40/IDOCTYP/text()");
            String logicalSystemId = getFromXPath(xmlDocument, "//IDOC/EDI_DC40/SNDPRN/text()");

            Source xml = new StreamSource(new ByteArrayInputStream(xmlData.getBytes()));
            Source xslt = xsltStore.readXSLT(idocType);

            if(xslt == null) {
                throw new XMLTransformException("No XSLT transformation file for IDOC type '" + idocType + "' found.");
            }

            StringWriter resultWriter = new StringWriter();
            Result out  = new StreamResult(resultWriter);

            TransformerFactory factory = TransformerFactory.newInstance();
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");

            Transformer transformer = factory.newTransformer(xslt);
            transformer.transform(xml, out);

            //TODO: find a better way to determine adapter endpoint
            if(idocType.equals("DELVRY03")) {
                return adapterClient.sendDespatchAdviceImportRequest(logicalSystemId, resultWriter.toString());
            } else if(idocType.equals("WBBDLD")) {
                return adapterClient.sendProductImportRequest(logicalSystemId, resultWriter.toString());
            } else {
                throw new TransformerException("Unable to determin adapter endpoint for IDOC type " + idocType);
            }

        } catch (TransformerException | ParserConfigurationException | SAXException | IOException e) {
            throw new XMLTransformException("Unable to transform IDOC to JSON", e);
        }
    }

    private String getFromXPath(Document document, String expression) throws XMLTransformException {
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression compile = xPath.compile(expression);

            return compile.evaluate(document);
        } catch (XPathExpressionException ex) {
            throw new XMLTransformException("Unable to parse IDOC type", ex);
        }
    }

    private Document getXmlDocument(String xmlData) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();

        return builder.parse(new InputSource(new StringReader(xmlData)));
    }
}
