package org.knowledge4retail.k4rsaperpadapter.exception;

public class XSLTWriteException extends Exception{

    public XSLTWriteException(String message) {
        super(message);
    }

    public XSLTWriteException(String message, Throwable cause) {
        super(message, cause);
    }

}
