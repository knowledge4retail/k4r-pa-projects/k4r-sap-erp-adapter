package org.knowledge4retail.k4rsaperpadapter.exception;

public class XMLTransformException extends Exception{

    public XMLTransformException(String message) {
        super(message);
    }

    public XMLTransformException(String message, Throwable cause) {
        super(message, cause);
    }

}
