package org.knowledge4retail.k4rsaperpadapter.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.k4rsaperpadapter.exception.XSLTWriteException;
import org.knowledge4retail.k4rsaperpadapter.service.xslt.ReadWriteXSLTStore;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class XSLTStorageController {

    private final ReadWriteXSLTStore xsltStore;

    public XSLTStorageController(ReadWriteXSLTStore xsltStore) {
        this.xsltStore = xsltStore;
    }

    @Operation(
            summary = "Uploads an XSLT file for a transforming a specific IDOC type",
            responses = {
                    @ApiResponse(responseCode = "201", description = "XSLT data successfully uploaded", content = @Content(schema = @Schema(implementation = String.class))),
                    @ApiResponse(responseCode = "400", description = "Request Body validation error")
            }
    )
    @PostMapping("api/v1/xslt/upload/{idocType}")
    public ResponseEntity<String> uploadXSLTString(@PathVariable("idocType") String type, @RequestBody String xslt) {
        log.info("Uploading xslt for type {}", type);

        try {
            xsltStore.writeXSLT(type, xslt);

            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (XSLTWriteException e) {
            log.error("Unable to save xslt data for idoc type '{}': {}", type, e.getMessage(), e);

            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
