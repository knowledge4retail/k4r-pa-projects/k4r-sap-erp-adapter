package org.knowledge4retail.k4rsaperpadapter.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.knowledge4retail.k4rsaperpadapter.exception.XMLTransformException;
import org.knowledge4retail.k4rsaperpadapter.model.ImportOperationResult;
import org.knowledge4retail.k4rsaperpadapter.service.ImportService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ImportIdocController {

    private final ImportService importService;

    public ImportIdocController(ImportService importService) {
        this.importService = importService;
    }

    @Operation(
            summary = "Imports an IDOC",
            responses = {
                    @ApiResponse(responseCode = "201", description = "IDOC successfully imported", content = @Content(schema = @Schema(implementation = ImportOperationResult.class))),
                    @ApiResponse(responseCode = "400", description = "Request Body validation error")
            }
    )
    @PostMapping(value = "api/v1/import/idoc", consumes = "application/xml")
    public ResponseEntity<ImportOperationResult> importData(@RequestBody String xmlData) throws XMLTransformException {
        return new ResponseEntity<>(importService.importData(xmlData), HttpStatus.CREATED);
    }
}
