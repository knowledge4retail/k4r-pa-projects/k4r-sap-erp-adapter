package org.knowledge4retail.k4rsaperpadapter.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.k4rsaperpadapter.exception.XMLTransformException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler {

    /**
     * Handles unknown Exception and server errors
     *
     * @param ex thrown exception
     * @param wr web request
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<DefaultError> unknownException(Exception ex, WebRequest wr) {
        log.error("Unable to handle {}", wr.getDescription(false), ex);

        return new ResponseEntity<>(new DefaultError(Exception.class.getSimpleName(), ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles EntityAlreadyExistsException
     *
     * @param ex thrown exception
     * @param wr web request
     */
    @ExceptionHandler(XMLTransformException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<DefaultError> xmlTransformationException(Exception ex, WebRequest wr) {
        log.error("Unable to handle {}", wr.getDescription(false), ex);

        return new ResponseEntity<>(new DefaultError(XMLTransformException.class.getSimpleName(), ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class DefaultError {
        private String type;
        private String message;
    }
}
